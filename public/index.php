<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Middleware\HttpBasicAuthentication;
use RaffleSimulation\Raffle\Raffle;
use RaffleSimulation\Raffle\Contestant;
use RaffleSimulation\Raffle\Storage\StorageInterface as RaffleStorage;
use RaffleSimulation\Raffle\Storage\Exception\RaffleNotInitialized;
use RaffleSimulation\Raffle\Exception\RegistrationIsClosed;
use RaffleSimulation\Raffle\Exception\ContestantAlreadyRegistered;

$container = require '../config/bootstrap.php';
$config = $container->get('config');

$app = new App($container);

$app->add(function (RequestInterface $request, ResponseInterface $response, callable $next) {
    if (session_status() != PHP_SESSION_ACTIVE) {
        session_name('RAFFLE');
        session_start();
    }

    return $next($request, $response);
});
$app->add(new HttpBasicAuthentication([
    'secure' => false,
    'path' => '/manage',
    'users' => $config['management']['users'],
]));

$app->get('/', function (Request $request, Response $response) {
    /* @var $raffleStorage RaffleStorage */
    $raffleStorage = $this->get('RaffleStorage');

    try {
        $raffle = $raffleStorage->get();

        $contestantId = session_id();

        $raffle->register(Contestant::create($contestantId));

        $raffleStorage->save($raffle);
    } catch (RaffleNotInitialized $ex1) {
        return $this->get('view')->render($response, 'error.html', [
            'message' => 'Raffle not opened',
        ]);
    } catch (RegistrationIsClosed $ex2) {
        return $this->get('view')->render($response, 'error.html', [
            'message' => 'Registration is closed',
        ]);
    } catch (ContestantAlreadyRegistered $ex3) {
    }

    return $this->get('view')->render($response, 'raffle.html');
})->setName('raffle');

$app->get('/result', function (Request $request, Response $response) {
    /* @var $raffleStorage RaffleStorage */
    $raffleStorage = $this->get('RaffleStorage');

    $lastCheck = $_SESSION['last_check'] ?? new DateTime();

    $_SESSION['last_check'] = new DateTime();

    try {
        $raffle = $raffleStorage->get();

        $result = 'idle';

        if ($raffle->getLastDrawAt() && $raffle->getLastDrawAt() > $lastCheck) {
            $contestantId = session_id();
            $contestant = Contestant::create($contestantId);

            if ($raffle->getCurrentWinner() && $raffle->getCurrentWinner()->getId() === $contestant->getId()) {
                $result = 'winner';
            } elseif ($raffle->isWinner($contestant)) {
                $result = 'done';
            } else {
                $result = 'miss';
            }
        }

        return $response->withJson([
            'result' => $result,
        ], 200);
    } catch (RaffleNotInitialized $ex) {
        return $response->withJson([
            'error' => 'Not opened',
        ], 400);
    }
})->setName('result');

$app->group('/manage', function () {
    $this->get('', function ($request, $response) {
        try {
            /* @var $raffle Raffle */
            $raffle = $this->get('RaffleStorage')->get();

            $raffleData = [
                'contestants' => $raffle->getContestants(),
                'contestants_count' => count($raffle->getContestants()),
                'winners' => $raffle->getWinners(),
                'winners_count' => count($raffle->getWinners()),
                'is_open' => $raffle->isOpen(),
                'is_closed' => $raffle->isClosed(),
                'is_completed' => $raffle->isCompleted(),
            ];
        } catch (RaffleNotInitialized $ex) {
            $raffleData = [
                'contestants' => [],
                'contestants_count' => 0,
                'winners' => [],
                'winners_count' => 0,
                'is_open' => false,
                'is_closed' => false,
                'is_completed' => false,
            ];
        }

        return $this->get('view')->render($response, 'manage.html', $raffleData);
    })->setName('manage');
    $this->post('/open', function ($request, $response) {
        $raffle = Raffle::open();

        $this->get('RaffleStorage')->save($raffle);

        return $response->withRedirect($this->get('router')->pathFor('manage'));
    })->setName('open');
    $this->post('/close', function ($request, $response) {
        /* @var $raffle Raffle */
        $raffle = $this->get('RaffleStorage')->get();

        $raffle->closeRegistrations();

        $this->get('RaffleStorage')->save($raffle);

        return $response->withRedirect($this->get('router')->pathFor('manage'));
    })->setName('close');
    $this->post('/draw', function ($request, $response) {
        /* @var $raffle Raffle */
        $raffle = $this->get('RaffleStorage')->get();

        $raffle->draw();

        $this->get('RaffleStorage')->save($raffle);

        return $response->withRedirect($this->get('router')->pathFor('manage'));
    })->setName('draw');
});

$app->run();
