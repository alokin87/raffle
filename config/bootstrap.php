<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

require '../vendor/autoload.php';

$config = is_file('config.php') ? require 'config.php' : require 'config.php.dist';

if (isset($config['php_settings'])) {
    foreach ($config['php_settings'] as $name => $value) {
        ini_set($name, $value);
    }
}

date_default_timezone_set('UTC');

$servicesConfig = require 'services.php';
$servicesConfig['config'] = $config;
$servicesConfig['settings'] = $config['settings'];

$container = new Slim\Container($servicesConfig);

return $container;
