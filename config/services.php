<?php

use Interop\Container\ContainerInterface;

return [
    'view' => function (ContainerInterface $container) {
        $config = $container->get('config');

        $view = new Slim\Views\Twig(
            [
                '../templates/',
                '../public/themes/' . $config['theme']
            ],
            [
                'cache' => false
            ]
        );

        $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
        $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

        return $view;
    },
    'RaffleStorage' => function () {
        return new RaffleSimulation\Raffle\Storage\FileStorage('../data/raffle');
    },
];
