<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulationTest\Raffle;

use PHPUnit\Framework\TestCase;
use RaffleSimulation\Raffle\Raffle;
use RaffleSimulation\Raffle\Contestant;
use RaffleSimulation\Raffle\Exception\RegistrationIsClosed;
use RaffleSimulation\Raffle\Exception\ContestantAlreadyRegistered;
use RaffleSimulation\Raffle\Exception\RegistrationsAreStillOpen;
use RaffleSimulation\Raffle\Exception\RaffleIsCompleted;
use RaffleSimulation\Raffle\Exception\RaffleIsEmpty;

class RaffleTest extends TestCase
{
    /**
     * @test
     */
    public function it_is_in_open_state_by_default()
    {
        $raffle = Raffle::open();

        $this->assertTrue($raffle->isOpen());
    }

    /**
     * @test
     */
    public function it_can_be_initialized_with_contestants()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);

        $this->assertCount(3, $raffle->getContestants());
    }

    /**
     * @test
     */
    public function it_provides_contestants()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
        ]);

        $contestants = $raffle->getContestants();
        $this->assertCount(2, $contestants);
        $this->assertInstanceOf(Contestant::class, current($contestants));
    }

    /**
     * @test
     */
    public function it_allows_for_contestants_registration()
    {
        $raffle = Raffle::open();

        $raffle->register(Contestant::create('1'));
        $raffle->register(Contestant::create('2'));

        $this->assertCount(2, $raffle->getContestants());
    }

    /**
     * @test
     */
    public function it_makes_sure_that_same_contestant_cannot_be_registered_twice()
    {
        $cheater = Contestant::create('2');

        $raffle = Raffle::open([
            Contestant::create('1'),
            $cheater
        ]);

        $this->expectException(ContestantAlreadyRegistered::class);

        $raffle->register($cheater);
    }

    /**
     * @test
     */
    public function it_can_be_closed_for_registrations()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);

        $raffle->closeRegistrations();

        $this->assertFalse($raffle->isOpen());
    }

    /**
     * @test
     */
    public function it_disallows_contestants_registration_if_closed()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);

        $raffle->closeRegistrations();

        $this->expectException(RegistrationIsClosed::class);

        $raffle->register(Contestant::create('4'));
    }

    /**
     * @test
     */
    public function it_draws_winner()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);
        $raffle->closeRegistrations();

        $winner = $raffle->draw();

        $this->assertInstanceOf(Contestant::class, $winner);
    }

    /**
     * @test
     */
    public function it_disallows_drawing_if_not_closed()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);

        $this->expectException(RegistrationsAreStillOpen::class);

        $raffle->draw();
    }

    /**
     * @test
     */
    public function it_makes_sure_same_winner_is_not_drawned_multiple_times()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
        ]);
        $raffle->closeRegistrations();

        $winner1 = $raffle->draw();
        $winner2 = $raffle->draw();

        $this->assertNotSame($winner1->getId(), $winner2->getId());
    }

    /**
     * @test
     */
    public function it_can_be_completed()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);
        $raffle->closeRegistrations();

        $raffle->draw();

        $raffle->complete();

        $this->assertTrue($raffle->isCompleted());
    }

    /**
     * @test
     */
    public function it_disallows_drawing_if_completed()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);
        $raffle->closeRegistrations();

        $raffle->draw();

        $raffle->complete();

        $this->expectException(RaffleIsCompleted::class);

        $raffle->draw();
    }

    /**
     * @test
     */
    public function it_fails_to_draw_if_out_of_contestants()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
        ]);
        $raffle->closeRegistrations();

        $raffle->draw();
        $raffle->draw();

        $this->expectException(RaffleIsEmpty::class);

        $raffle->draw();
    }

    /**
     * @test
     */
    public function it_provides_winners()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);
        $raffle->closeRegistrations();

        $raffle->draw();
        $raffle->draw();

        $winners = $raffle->getWinners();

        $this->assertCount(2, $winners);
    }

    /**
     * @test
     */
    public function it_checks_if_contestant_is_winner()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);
        $raffle->closeRegistrations();

        $winner = $raffle->draw();

        $this->assertTrue($raffle->isWinner($winner));
    }

    /**
     * @test
     */
    public function it_updates_updated_at_after_draw()
    {
        $raffle = Raffle::open([
            Contestant::create('1'),
            Contestant::create('2'),
            Contestant::create('3'),
        ]);

        $this->assertNull($raffle->getLastDrawAt());

        $raffle->closeRegistrations();
        $raffle->draw();

        $this->assertNotNull($raffle->getLastDrawAt());
    }
}
