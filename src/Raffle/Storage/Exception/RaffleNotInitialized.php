<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulation\Raffle\Storage\Exception;

class RaffleNotInitialized extends \RuntimeException implements ExceptionInterface
{
}
