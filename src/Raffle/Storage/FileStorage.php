<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulation\Raffle\Storage;

use RaffleSimulation\Raffle\Raffle;
use RaffleSimulation\Raffle\Storage\Exception\RaffleNotInitialized;

final class FileStorage implements StorageInterface
{
    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $basePath)
    {
        $this->filePath = rtrim($basePath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'raffle.dat';
    }

    public function save(Raffle $raffle)
    {
        file_put_contents($this->filePath, serialize($raffle));
    }

    public function get() : Raffle
    {
        if (!is_file($this->filePath)) {
            throw new RaffleNotInitialized();
        }

        $raffleSerialized = file_get_contents($this->filePath);

        return unserialize($raffleSerialized);
    }
}
