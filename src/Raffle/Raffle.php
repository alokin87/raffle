<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulation\Raffle;

use RaffleSimulation\Raffle\Exception\RegistrationIsClosed;
use RaffleSimulation\Raffle\Exception\ContestantAlreadyRegistered;
use RaffleSimulation\Raffle\Exception\RegistrationsAreStillOpen;
use RaffleSimulation\Raffle\Exception\RaffleIsCompleted;
use RaffleSimulation\Raffle\Exception\RaffleIsEmpty;
use DateTimeImmutable;

class Raffle
{
    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';
    const STATUS_COMPLETED = 'completed';

    /**
     * @var Contestant[]
     */
    protected $contestants = [];

    /**
     * @var string
     */
    protected $status;

    /**
     * @var Contestant
     */
    protected $currentWinner;

    /**
     * @var Contestant[]
     */
    protected $winners = [];

    /**
     * @var DateTimeImmutable
     */
    protected $lastDrawAt;

    protected function __construct(array $contestants, string $status)
    {
        $this->contestants = $contestants;
        $this->status = $status;
    }
    
    public static function open(array $contestants = []) : self
    {
        $raffle = new self([], self::STATUS_OPEN);

        foreach ($contestants as $contestant) {
            $raffle->register($contestant);
        }

        return $raffle;
    }

    public function register(Contestant $contestant)
    {
        if (!$this->isOpen()) {
            throw new RegistrationIsClosed();
        }

        $contestantId = $contestant->getId();

        if (array_key_exists($contestantId, $this->contestants)) {
            throw ContestantAlreadyRegistered::forContestant($contestant);
        }

        $this->contestants[$contestantId] = $contestant;
    }

    public function closeRegistrations()
    {
        $this->status = self::STATUS_CLOSED;
    }

    public function getContestants() : array
    {
        return $this->contestants;
    }
    
    public function draw() : Contestant
    {
        if ($this->isOpen()) {
            throw new RegistrationsAreStillOpen();
        }

        if ($this->isCompleted()) {
            throw new RaffleIsCompleted();
        }

        if (0 === count($this->contestants)) {
            throw new RaffleIsEmpty();
        }

        $contestantKey = array_rand($this->contestants);

        $contestant = $this->contestants[$contestantKey];

        unset($this->contestants[$contestantKey]);

        $this->currentWinner = $contestant;
        $this->winners[$contestant->getId()] = $contestant;

        $this->lastDrawAt = new DateTimeImmutable();

        return $contestant;
    }

    public function complete()
    {
        $this->status = self::STATUS_COMPLETED;
    }

    public function getCurrentWinner()
    {
        return $this->currentWinner;
    }

    public function getWinners() : array
    {
        return array_values($this->winners);
    }

    public function isWinner(Contestant $contestant) : bool
    {
        return array_key_exists($contestant->getId(), $this->winners);
    }

    public function isOpen() : bool
    {
        return self::STATUS_OPEN === $this->status;
    }

    public function isClosed() : bool
    {
        return self::STATUS_CLOSED === $this->status;
    }

    public function isCompleted() : bool
    {
        return self::STATUS_COMPLETED === $this->status;
    }

    public function getLastDrawAt()
    {
        return $this->lastDrawAt;
    }
}
