<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulation\Raffle;

class Contestant
{
    /**
     * @var string
     */
    protected $id;
    
    protected function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function create(string $id) : self
    {
        return new self($id);
    }

    public function getId() : string
    {
        return $this->id;
    }
}
