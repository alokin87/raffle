<?php

declare(strict_types=1);

/**
 * This file is part of the Raffle package.
 *
 * Copyright (c) Nikola Posa <posa.nikola@gmail.com>
 *
 * For full copyright and license information, please refer to the LICENSE file,
 * located at the package root folder.
 */

namespace RaffleSimulation\Raffle\Exception;

use DomainException;
use RaffleSimulation\Raffle\Contestant;

class ContestantAlreadyRegistered extends DomainException implements ExceptionInterface
{
    public static function forContestant(Contestant $contestant) : self
    {
        return new self(sprintf(
            "Contestant '%' has already been registered",
            $contestant->getId()
        ));
    }
}
